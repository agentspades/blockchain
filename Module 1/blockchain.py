import hashlib
import json
from os import path
import os
import time
from crypt import methods

from flask import Flask, jsonify, request

currentDir = os.path.dirname(os.path.realpath(__file__))

if not path.exists(f"{currentDir}/blockdata"):
    os.makedirs(f"{currentDir}/blockdata")

path = f"{currentDir}/blockdata/"


class Blockchain:
    def __init__(self):
        self.people = []
        self.chain = []
        self.count = 0
        self.msg = ""
        self.sig = ""
        files = os.listdir(path)
        files.sort()
        try:
            if len(files) > 0:
                for file in files:
                    with open(f"{path}{file}", "rt") as f:
                        lastBlock = f.readlines()[-1]
                        self.count = json.loads(lastBlock)["index"]
                        if len(self.chain) == 0:
                            self.chain.append(json.loads(lastBlock))
                        else:
                            self.chain[0] = json.loads(lastBlock)
            else:
                raise Exception()
        except:
            self.createBlock(proof=1, prevHash="0")

    def createBlock(self, proof, prevHash):
        if prevHash == "0":
            index = 0
        else:
            index = self.count + 1
        block = {
            "index": index,
            "timestamp": str(int(time.time())),
            "proof": proof,
            "prevHash": prevHash,
            "msg": self.msg,
            "sig": self.sig,
        }
        if len(self.chain) == 0:
            self.chain.append(block)
        return block

    def getLastBlock(self):
        return self.chain[-1]

    def getDifficulty(self, height):
        if height < 5000:
            return ["0000", "20"]
        elif height < 10000:
            return ["00000", "18"]
        elif height < 50000:
            return ["000000", "15"]
        elif height < 100000:
            return ["0000000", "13"]
        elif height < 500000:
            return ["00000000", "10"]
        elif height < 1000000:
            return ["000000000", "8"]
        elif height > 1000000:
            return ["0000000000", "5"]

    def proofOfWork(self, prevHash):
        newProof = 1
        solved = False
        difficulty = self.getDifficulty(len(self.chain))
        while solved == False:
            block = self.createBlock(newProof, prevHash)
            hashOperation = hashlib.sha256(
                json.dumps(block, sort_keys=True).encode()
            ).hexdigest()
            if hashOperation[: len(difficulty[0])] == difficulty[0]:
                self.chain.append(block)
                solved = True
            else:
                newProof += 1
        if len(self.chain) >= 1000:
            with open(f"{path}{str(int(time.time()))}.blk", "a") as f:
                for item in self.chain:
                    if item == self.chain[0]:
                        continue
                    f.write("%s\n" % json.dumps(item))
            while len(self.chain) > 1:
                self.chain.pop(-1)
        self.count += 1
        return block

    def hash(self, block):
        blockStr = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(blockStr).hexdigest()

    def isChainValid(self, chain):
        prevBlock = chain[0]
        blockIndex = 1
        while blockIndex < len(chain):
            block = chain[blockIndex]
            if block["prevHash"] != self.hash(prevBlock):
                return False
            hashOperation = self.hash(block)
            diff = self.getDifficulty(blockIndex)[0]
            if hashOperation[: len(diff)] != diff:
                return False
            prevBlock = block
            blockIndex += 1
        return True

    def search(self, q, round):
        blockIndex = 1
        foundBlocks = []
        files = os.listdir(path)
        files.sort()
        if len(files) > 0:
            for file in files:
                with open(f"{path}{file}", "rt") as f:
                    lines = f.readlines()
                    for line in lines:
                        block = json.loads(line)
                        message = block["msg"]
                        if (
                            q.lower() in message["patientName"].lower()
                            and message["round"].lower() == round.lower()
                        ):
                            foundBlocks.append(block)
        while blockIndex < len(self.chain):
            block = self.chain[blockIndex]
            message = block["msg"]
            if (
                q.lower() in message["patientName"].lower()
                and message["round"].lower() == round.lower()
            ):
                foundBlocks.append(block)
            blockIndex += 1
        if len(foundBlocks) > 0:
            return foundBlocks
        return "No results"


app = Flask(__name__)

blockchain = Blockchain()


# @app.route("/mine", methods=['GET'])
# def mine():
#     prevBlock = blockchain.getLastBlock()
#     prevHash = blockchain.hash(prevBlock)
#     reward = blockchain.getDifficulty(len(blockchain.chain))[1]
#     block = blockchain.proofOfWork(prevHash)
#     res = {
#         "Block": block,
#         "Reward": reward
#     }
#     return jsonify(res), 200


@app.route("/chain", methods=["GET"])
def list():
    res = {"chain": blockchain.chain, "length": len(blockchain.chain)}
    return jsonify(res), 200


@app.route("/isValid", methods=["GET"])
def isValid():
    valid = blockchain.isChainValid(blockchain.chain)
    res = {"Valid": valid}
    return jsonify(res), 200


# /search?q=<search text>&rnd<med round>
@app.route("/search", methods=["GET"])
def search():
    q = request.args.get("q", default="", type=str)
    rnd = request.args.get("rnd", default="", type=str)
    if len(q) > 0 and len(rnd) > 0:
        res = blockchain.search(q, rnd)
        status = 200
    else:
        res = "Not a valid request"
        status = 400
    return jsonify(res), status


# /add?msg=<message>&sig=<signature>
@app.route("/add", methods=["GET"])
def add():
    blockchain.msg = json.loads(request.args.get("msg", default="", type=str))
    blockchain.sig = request.args.get("sig", default="", type=str)
    if len(blockchain.msg) > 0 and len(blockchain.sig) > 0:
        prevBlock = blockchain.getLastBlock()
        prevHash = blockchain.hash(prevBlock)
        blockchain.proofOfWork(prevHash)
        blockchain.sig = ""
        blockchain.msg = ""
        res = "Msg added"
    else:
        res = "Msg not added"
    return res, 200


app.run()
