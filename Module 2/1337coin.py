from crypt import methods
import time
import hashlib
import json
from pickle import FALSE, GET
from flask import Flask, jsonify, request
import requests
from uuid import uuid4
from urllib.parse import urlparse


class Blockchain:
    def __init__(self):
        self.chain = []
        self.transactions = []
        self.createBlock(proof=1, prevHash='0')
        self.nodes = set()

    def createBlock(self, proof, prevHash):
        transactions = []
        if len(self.transactions) > 0:
            transactions = self.transactions
        block = {'index': len(self.chain) + 1,
                 'timestamp': str(int(time.time())),
                 'proof': proof,
                 'transactions': transactions,
                 'prevHash': prevHash}
        if len(self.chain) == 0:
            self.chain.append(block)
        return block

    def getLastBlock(self):
        return self.chain[-1]

    def getDifficulty(self, height):
        if height < 5:
            return ['0000', '20']
        elif height < 10000:
            return ['00000', '18']
        elif height < 50000:
            return ['000000', '15']
        elif height < 100000:
            return ['0000000', '13']
        elif height < 500000:
            return ['00000000', '10']
        elif height < 1000000:
            return ['000000000', '8']
        elif height > 1000000:
            return ['0000000000', '5']

    def proofOfWork(self, prevHash):
        newProof = 1
        solved = False
        difficulty = self.getDifficulty(len(self.chain))
        while solved == False:
            block = self.createBlock(newProof, prevHash)
            hashOperation = hashlib.sha256(json.dumps(
                block, sort_keys=True).encode()).hexdigest()
            if hashOperation[:len(difficulty[0])] == difficulty[0]:
                self.chain.append(block)
                solved = True
            else:
                newProof += 1
        self.transactions = []
        return block

    def hash(self, block):
        blockStr = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(blockStr).hexdigest()

    def isChainValid(self, chain):
        prevBlock = chain[0]
        blockIndex = 1
        while blockIndex < len(chain):
            block = chain[blockIndex]
            if block['prevHash'] != self.hash(prevBlock):
                return False
            hashOperation = self.hash(block)
            diff = self.getDifficulty(blockIndex)[0]
            if hashOperation[:len(diff)] != diff:
                return False
            prevBlock = block
            blockIndex += 1
        return True

    def addTransaction(self, tx, rx, amount):
        self.transactions.append({'time': str(int(time.time())),
                                  'to': tx,
                                  'from': rx,
                                  'amount': amount})
        prevBlock = self.getLastBlock()
        return prevBlock['index'] + 1

    def addNode(self, address):
        url = urlparse(address)
        self.nodes.add(url.netloc)

    def replaceChain(self):
        network = self.nodes
        longestChain = None
        maxLength = len(self.chain)
        for node in network:
            res = requests.get(f'http://{node}/chain')
            if res.status_code == 200:
                length = res.json()['length']
                chain = res.json()['chain']
                if maxLength < length and self.isChainValid(chain):
                    maxLength = length
                    longestChain = chain
        if longestChain:
            self.chain = longestChain
            return True
        return False


nodeAddress = str(uuid4()).replace('-', '')

blockchain = Blockchain()

app = Flask(__name__)


@app.route("/mine", methods=['GET'])
def mine():
    prevBlock = blockchain.getLastBlock()
    prevHash = blockchain.hash(prevBlock)
    reward = blockchain.getDifficulty(len(blockchain.chain))[1]
    # blockchain.addTransaction("BURN", nodeAddress, reward)
    block = blockchain.proofOfWork(prevHash)
    res = {
        "Block": block,
        "Reward": reward
    }
    return jsonify(res), 200


@app.route("/chain", methods=['GET'])
def list():
    res = {"chain": blockchain.chain,
           "length": len(blockchain.chain)}
    return jsonify(res), 200


@app.route("/isValid", methods=['GET'])
def isValid():
    valid = blockchain.isChainValid(blockchain.chain)
    res = {"Valid": valid}
    return jsonify(res), 200


@app.route("/tx", methods=['POST'])
def addTx():
    json = request.get_json()
    keys = ['to', 'from', 'amount']
    if not all(key in json for key in keys):
        return "ERROR: Invalid JSON", 400
    index = blockchain.addTransaction(json['to'], json['from'], json['amount'])
    res = {'Block height': index}
    return jsonify(res), 201


@app.route("/newNode", methods=['POST'])
def newNode():
    json = request.get_json()
    nodes = json.get('nodes')
    if nodes is None:
        return "ERROR: No nodes", 400
    for node in nodes:
        blockchain.addNode(node)
    res = {'nodes': list(blockchain.nodes)}
    return jsonify(res), 201


@app.route("/newChain", methods=['GET'])
def newChain():
    curHeight = len(blockchain.chain)
    valid = blockchain.replaceChain()
    if valid:
        res = {'Message': 'Current chain is latest',
               'Height': curHeight}
    else:
        res = {"Message": 'Chain was updated',
               'Old Height': curHeight,
               'New Height': len(blockchain.chain)}
    return jsonify(res), 200


app.run()
